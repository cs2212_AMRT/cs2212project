package testHarness;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import offerings.CourseOffering;
import offerings.ICourseOffering;
import offerings.OfferingFactory;
import operations.*;
import registrar.ModelRegister;
import systemUsers.StudentModel;
import authenticatedUsers.LoggedInAuthenticatedUser;
import operations.Startstop;

public class TestStudentModelFactory_1 {

	//	public static void main(String[] args) throws IOException{
	//		// TODO Auto-generated method stub
	//		SystemUserModelFactory factory = new StudentModelFactory();
	//		BufferedReader br = new BufferedReader(new FileReader(new File("note_1.txt")));
	//		factory.createSystemUserModel(br, null);
	//	}



	public static void main(String[] args) throws IOException{

		//		Create an instance of an OfferingFactory
		OfferingFactory factory = new OfferingFactory();
		BufferedReader br = new BufferedReader(new FileReader(new File("note_1.txt")));
		//		Use the factory to populate as many instances of courses as many files we've got.
		CourseOffering	courseOffering = factory.createCourseOffering(br);
		br.close();
		//		Loading 1 file at a time
		br = new BufferedReader(new FileReader(new File("note_2.txt")));
		//		here we have only two files
		courseOffering = factory.createCourseOffering(br);
		br.close();
		//		code to perform sanity checking of all our code
		//		by printing all of the data that we've loaded
		for(CourseOffering course : ModelRegister.getInstance().getAllCourses()){
			System.out.println("ID : " + course.getCourseID() + "\nCourse name : " + course.getCourseName() + "\nSemester : " + 
					course.getSemester());
			System.out.println("Students allowed to enroll\n");
			for(StudentModel student : course.getStudentsAllowedToEnroll()){
				System.out.println("Student name : " + student.getName() + "\nStudent surname : " + student.getSurname() + 
						"\nStudent ID : " + student.getID() + "\nStudent EvaluationType : " + 
						student.getEvaluationEntities().get(course) + "\n\n");
			}

			for(StudentModel student : course.getStudentsAllowedToEnroll()){
				for(ICourseOffering course2 : student.getCoursesAllowed())
					System.out.println(student.getName() + "\t\t -> " + course2.getCourseName());
			}
		}

		//Our main begins here

		Startstop checkStatus = Startstop.getInstance();

		System.out.println("Please login to begin the system.");
		Login loggedIn = new Login();
		LoggedInAuthenticatedUser admin = loggedIn.performOp();
		if(checkStatus.systemState() == true){
			System.out.println("\nThe student will now login.");
			LoggedInAuthenticatedUser student = loggedIn.performOp();

			System.out.println("\nYou will now enroll in a class.");
			EnrollInClass newEnrollment = new EnrollInClass();
			newEnrollment.performOp(student);

			System.out.println("\nThe student will now change its notification settings.");
			SetNotificationType notiftype = new SetNotificationType();
			notiftype.performOp(student);

			System.out.println("\nThe instructor will now login.");
			LoggedInAuthenticatedUser instructor = loggedIn.performOp();

			System.out.println("\nThe instructor will now add marks.");
			AddMarks addMarks = new AddMarks();
			for (int i = 0; i < 2; i++) {
				addMarks.performOp(instructor);
			}

			System.out.println("\nThe student will now print it's record.");
			StudentPrintRecord printer = new StudentPrintRecord();
			printer.performOp(student);

			System.out.println("\nThe instructor will not print a record for all students in a given class.");
			InstructorPrintRecord printerTwo = new InstructorPrintRecord();
			printerTwo.performOp(instructor);
		}
		else{
			System.out.println("The system is not on, all following operations cannot continue.");
		}


	}
}
