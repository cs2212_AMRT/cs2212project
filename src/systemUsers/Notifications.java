package systemUsers;

import customDatatypes.NotificationTypes;
import systemUsers.StudentModel;

public class Notifications {
	public static void sendNotification(StudentModel student) {
       //Sends a notification when a mark is added or modified based on the students notification preference
		NotificationTypes notificationType = student.getNotificationType();
		if (notificationType == null) {
			// This means notifications have not been initialized
			return;
		}
        switch (notificationType) {
            default:
            	break;
            case EMAIL:
                System.out.println("Recieved an Email Mark Update ");
                break;
            case CELLPHONE:
                System.out.println("Recieved a Cellphone Mark Update ");
                break;
            case PIGEON_POST:
                System.out.println("Recieved a Pigeon Post Mark Update ");
                break;
        }
    }
}


