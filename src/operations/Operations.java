package operations;

import authenticatedUsers.LoggedInAuthenticatedUser;

public interface Operations {

	// This is the interface for all the operations that will be done
	public void performOp(LoggedInAuthenticatedUser user);
	
	public LoggedInAuthenticatedUser performOp();
	
}
