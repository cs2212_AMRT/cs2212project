package operations;
import authenticatedUsers.LoggedInAuthenticatedUser;

public class Startstop implements Operations
{
    private static Startstop firstInstance = null;
    private boolean state;
    private Startstop()
    {

    }

    public static Startstop getInstance()
    {
        if (null == firstInstance)
        {
            firstInstance = new Startstop();
        }

        return firstInstance;
    }

    public void readySystem(LoggedInAuthenticatedUser user)
    {
        if(user.getAuthenticationToken().getUserType().equals("Admin"))
        {
            Startstop.getInstance().startSystem();
        }
    }

    public void unreadySystem(LoggedInAuthenticatedUser user)
    {
        if(user.getAuthenticationToken().getUserType().equals("Admin"))
        {
            Startstop.getInstance().stopSystem();
        }
    }

    public void startSystem()
    {
        state = true;
    }

    public void stopSystem()
    {
        state = false;
    }

    public boolean systemState()
    {
        return state;
    }

    public void performOp(LoggedInAuthenticatedUser user){
    }
    public LoggedInAuthenticatedUser performOp() {
        return null;
    }
}

    
    