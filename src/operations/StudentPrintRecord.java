package operations;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

import authenticatedUsers.LoggedInAuthenticatedUser;
import customDatatypes.Marks;
import offerings.CourseOffering;
import offerings.ICourseOffering;
import registrar.ModelRegister;
import systemUsers.StudentModel;

public class StudentPrintRecord implements Operations {

	public void performOp(LoggedInAuthenticatedUser user) {

		System.out.println("You are now printing the student record");

		// Get the model register and then get the student from the register itself
		StudentModel student = (StudentModel)ModelRegister.getInstance().getRegisteredUser(user.getID());

		// Initialize the scanner to read user input
		boolean courseFound = false;
		Scanner reader = new Scanner(System.in);
		while (courseFound == false) {
			System.out.print("Enter the courseID of the course you want to print a record for: ");
			String usercourseID = reader.nextLine();

			// Loop through courses enrolled in until we get to the one the user specified
			for (ICourseOffering tempCourse : student.getCoursesEnrolled()){
				if (tempCourse.getCourseID().equals(usercourseID)){

					courseFound = true;
					System.out.println("\n____________________________________________\n" + tempCourse.getCourseName());
					System.out.println("This course is in semester: " + tempCourse.getSemester());
					System.out.println("The evaluation type for this course is: " + student.getEvaluationEntities().get(tempCourse));
					System.out.println("Your current record for this course:");
					student.getPerCourseMarks().get(tempCourse).initializeIterator();
					Marks mark = student.getPerCourseMarks().get(tempCourse);
					
					boolean hasNext = true;
					while (student.getPerCourseMarks().get(tempCourse).hasNext())
					{
						
						// Print out the student record
						mark.next();
						System.out.println(mark.getCurrentKey() + ": " + mark.getCurrentValue());
					}
				}
			}
			if (courseFound == false) {
				System.out.println("A course with that ID could not be found, please try again.");
			}
		}
	}

	public LoggedInAuthenticatedUser performOp() {
		return null;
	}

}
