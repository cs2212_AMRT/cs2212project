package operations;

import authenticatedUsers.LoggedInAuthenticatedUser;
import customDatatypes.NotificationTypes;
import registrar.ModelRegister;
import systemUsers.StudentModel;
import java.util.Scanner;


public class SetNotificationType implements Operations {

    private String type;

    private String notif;

    public static boolean donotif;

    private LoggedInAuthenticatedUser user;

    /**
     * Prompts the user to select/change their notification preferences
     *
     * @param user User that wants to perform the operation
     */

    public LoggedInAuthenticatedUser performOp(){
        return null;
    }

    public void performOp(LoggedInAuthenticatedUser user) {

        System.out.println("You are now choosing a notification type");

        this.user = user;

        //Checks to see if the user is a student
        if (user.getAuthenticationToken().getUserType().equals("Student")) {
            //Sets the notification variable to true
            donotif = true;

            //Get user input
            Scanner scanner = new Scanner(System.in);
            StudentModel student = (StudentModel) ModelRegister.getInstance().getRegisteredUser(user.getID());

            System.out.println("Notification Types: \n1. EMAIL\n2. CELLPHONE\n3. PIGEON_POST\n4. No Notification\n");
            System.out.print("Select Notification type: ");
            type = scanner.nextLine();

            //If statement that selects the notification type
            if (type.equals("1")){
                notif = "EMAIL";
            }
            else if(type.equals("2")){
                notif = "CELLPHONE";
            }
            else if(type.equals("3")){
                notif = "PIGEON_POST";
            }
            else{
                donotif = false;
                //This won't ever run but putting it here so we won't get an error
                notif = "EMAIL";
            }

            // Change notification preferences
            student.setNotificationType(NotificationTypes.valueOf(notif));
            //System.out.println(student.getNotificationType());
        }
    }
}
