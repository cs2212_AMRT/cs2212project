package operations;

import authenticatedUsers.LoggedInAuthenticatedUser;
import offerings.CourseOffering;
import offerings.ICourseOffering;
import registrar.ModelRegister;
import systemUsers.StudentModel;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EnrollInClass implements Operations {

	public LoggedInAuthenticatedUser performOp() {
		return null;
	}

	public void performOp(LoggedInAuthenticatedUser user) {

		System.out.println("You are now enrolling in a class");
	    //Check if the user is a student
		if (!user.getAuthenticationToken().getUserType().equals("Student")){
			System.out.println("You need to be a student to enroll in a course");
		} else {

			//Go to model and grab the student with the same ID as the user
			ModelRegister mr = ModelRegister.getInstance();
			StudentModel student = (StudentModel) mr.getRegisteredUser(user.getID());

			//Start reading in user input
			Scanner reader = new Scanner(System.in);

			boolean looper = true;
			while (looper == true) {
				System.out.print("Enter the courseID of the course you want to enroll in: ");
				String usercourseID = reader.nextLine();

				// If statement to decide whether to exit the enroll in class method or continue on
				if (usercourseID.equals("exit") || usercourseID.equals("Exit")) {
					looper = false;
				} else {

					// Check if user is already enrolled in the course.
					boolean enrolled = false;
					try {
						for (ICourseOffering tempcourse : student.getCoursesEnrolled()){
							if (tempcourse.getCourseID().equals(usercourseID)){
								enrolled = true;
								
								System.out.println("You are already enrolled in " + usercourseID + ".");
							}
						}
					} catch(NullPointerException ex) {
						
					}

					// If the user is not enrolled in the course then enrolled them in the course
					if (enrolled == false) {

						// Loop through all courses allowed for the user until the course is reached
						for (ICourseOffering tempcourse : student.getCoursesAllowed()){
							if (tempcourse.getCourseID().equals(usercourseID)){

								// If the student has not been enrolled in a course then we need to initialize the coursesEnrolled list
								if (student.getCoursesEnrolled() == null) {
									//List<ICourseOffering> enrolledCourses = new ArrayList<ICourseOffering>();
									student.setCoursesEnrolled(new ArrayList<ICourseOffering>());
								}
								
								// If there is a match in course IDs then enroll the student in the course
								student.getCoursesEnrolled().add(tempcourse);
								tempcourse.getStudentsEnrolled().add(student);
								System.out.println("You are now enrolled in " + usercourseID + ".");
								enrolled = true;
								looper = false;
							}
						}

						// If there were no courses with the ID input by the user then relay an error message and prompt the user to restart or exit
						if (enrolled == false) {
							System.out.println("Incorrect courseID, please try again or type 'Exit' to cancel.");
						}
					}
				}
			}
		}
	}
}
