package operations;

import authenticatedUsers.LoggedInAuthenticatedUser;
import authenticationServer.AuthenticationToken;
import loggedInUserFactory.LoggedInUserFactory;
import java.util.Scanner;
import java.io.*;

public class Login implements Operations {

	// Set attributes that will be used for the log in process
	private String firstName;
	private String surname;
	private String id;
	private LoggedInAuthenticatedUser loggedInUser;
	private AuthenticationToken userToken;

	public LoggedInAuthenticatedUser performOp() {

		System.out.print("You are now logging in\n");

		// Create more variables that will be used for the log in process, specifically for this scope
		Startstop checkStatus = Startstop.getInstance();

		String line = null;
		String fileName = "users.txt";
		userToken = new AuthenticationToken();
		boolean userFound = false;

		// Reading from System.in
		Scanner reader = new Scanner(System.in);
		while(userFound != true) {

			// Ask for user's credentials for logging in
			System.out.print("Enter your first name: ");
			firstName = reader.next();
			System.out.print("Enter your surname: ");
			surname = reader.next();
			System.out.print("Enter your ID: ");
			id = reader.next();

			// A try-catch block to catch any errors or exceptions we may get.
			try {

				// Read the file and use a bufferedReader to go through the file
				FileReader fileReader = new FileReader(fileName);
				BufferedReader bufferedReader = new BufferedReader(fileReader);

				// Go through the file line by line, looking for matching credentials
				while((line = bufferedReader.readLine()) != null) {

					String[] words = line.split("\t");
					if (words[0].equals(firstName)) {
						// First name is correct
						if (words[1].equals(surname)) {
							// Surname is correct
							if (id.equals(words[2])) {
								// ID is correct, user can now attempt to log in
								
								// If the system has not been turned on then we have to make sure no one but the admin may log in
								if (checkStatus.systemState() == false && words[3].equals("Admin") == false) {
									System.out.println("The administrator must log in and start the system");
									break;
								}
								if (checkStatus.systemState() == false && words[3].equals("Admin") == true) {
									checkStatus.startSystem();
									System.out.println("The system has been started by the administrator.");
								}
								if (checkStatus.systemState() == true) {

									// Assign attributes to authenticationToken
									userToken.setUserType(words[3]);
									userToken.setTokenID(Integer.parseInt(words[2]));

									userFound = true;
								} else {
									break;
								}
							}
						}
					}
				}

				if (userFound == false && bufferedReader.readLine() == null) {
					// Print out a response if no student exists with the credentials given
					System.out.println("User not found or incorrect credentials, please try again.");
				}
				// Close the file after we are done with it
				bufferedReader.close();
			}
			catch(FileNotFoundException ex) {
				System.out.println("Unable to open file '" + fileName + "'");
			}
			catch(IOException ex) {
				System.out.println("Error reading file '" + fileName + "'");
			}
		}
		// Close the scanner
		//reader.close();

		// Create the user using the authentication token that was just created.
		LoggedInUserFactory userFactory = new LoggedInUserFactory();
		LoggedInAuthenticatedUser user = userFactory.createAuthenticatedUser(userToken);

		// Continue on creating the user
		performOp(user);
		checkStatus.readySystem(loggedInUser);
		System.out.println(firstName + " " + surname + ", you are now logged in as: " + userToken.getUserType() + ".");
		return loggedInUser;

	}

	public void performOp(LoggedInAuthenticatedUser user) {

		// Set the information for the user
		user.setName(firstName);
		user.setSurname(surname);
		user.setID(id);
		user.setAuthenticationToken(userToken);

		loggedInUser = user;
	}

	public LoggedInAuthenticatedUser getUser() {
		// Returns the user
		return loggedInUser;
	}

}