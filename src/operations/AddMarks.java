package operations;

import java.io.FileNotFoundException;

import java.io.IOException;
import authenticatedUsers.LoggedInAuthenticatedUser;
import customDatatypes.EvaluationTypes;
import customDatatypes.Marks;
import customDatatypes.NotificationTypes;
import offerings.CourseOffering;
import offerings.ICourseOffering;
import registrar.ModelRegister;
import systemUsers.InstructorModel;
import systemUsers.StudentModel;
import systemUsers.SystemUserModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import authenticatedUsers.LoggedInAuthenticatedUser;

public class AddMarks implements Operations {

	private String courseID, studentID, eval;
	private double mark;


	public void performOp(LoggedInAuthenticatedUser user) {

		System.out.println("You are now adding marks\n");

		//Check to see if the user is an Instructor
		if (user.getAuthenticationToken().getUserType().equals("Instructor")) {

			// Start the scanner to get user input
			Scanner scanner = new Scanner(System.in);
			boolean courseFound = false;
			while(courseFound == false) {
				System.out.print("Enter the Course ID to add marks to: ");
				courseID = scanner.nextLine();

				// Checks to see if the course exists
				if (ModelRegister.getInstance().getRegisteredCourse(courseID) != null) {
					courseFound = true;
				} else {
					if (courseFound == false) {
						System.out.println("Course ID could not be found, please try again.");
					}
				}
			}

			//Check to see if the course entered is taught by the instructor
			InstructorModel instructor = (InstructorModel)ModelRegister.getInstance().getRegisteredUser(user.getID());
			CourseOffering course = ModelRegister.getInstance().getRegisteredCourse(courseID);

			// Loop through all instructors for the course to see if the really teaches the course.
			for (InstructorModel courseInstructor : course.getInstructor()) {
				if (courseInstructor != instructor) {
					System.out.println("Only the instructor of " + courseID + " may add marks.");
					return;
				}
			}

			// Get the student ID and check if the student exists
			boolean studentFound = false;
			while(studentFound == false) {
				System.out.print("Enter the Student ID that will be having a mark added: ");
				studentID = scanner.nextLine();
				// Checks to see if the student exists
				if (ModelRegister.getInstance().getRegisteredUser(studentID) != null) {
					studentFound = true;
				} else {
					if (studentFound == false) {
						System.out.println("Student ID could not be found, please try again.");
					}
				}
			}


			//Find student using register and get ready to add the marks
			StudentModel student = (StudentModel)ModelRegister.getInstance().getRegisteredUser(studentID);
			System.out.println("Student selected: " + student.getName() + " " + student.getSurname());
			if (student.getCoursesEnrolled() != null && student.getCoursesEnrolled().contains(course)) {

				// Initialize the marks map if a mark has never been added.
				Map<ICourseOffering, Marks> courseMarks;
				Marks marks;
				if (student.getPerCourseMarks() != null) {
					courseMarks = student.getPerCourseMarks();
					if (student.getPerCourseMarks().get(course) == null) {
						marks = new Marks();
					} else {
						marks = student.getPerCourseMarks().get(course);
					}
				} else {
					
					courseMarks = new HashMap<ICourseOffering, Marks>();
					marks = new Marks();
				}

				// Get user input for the evaluation strategy and the mark for it
				System.out.print("Please type the evaluation strategy (ex. Assignment x, Exam, Project, etc.: ");
				eval = scanner.nextLine();
				System.out.print("Please enter Mark: ");
				String markString = scanner.nextLine();
				mark = Double.parseDouble(markString);

				// The mark cannot be less than 0 or greater than 100
				if ((mark <= 100.0) && (mark >= 0.0)) {
					marks.addToEvalStrategy(eval, mark);
				} else {
					System.out.println("Mark must be between 0 and 100.");
					return;
				}

				// If there is no map for this student's course's marks then we need to initialize the map
				if (student.getPerCourseMarks() == null) {
					courseMarks.put(course, marks);
					student.setPerCourseMarks(courseMarks);
				} else {
					courseMarks.put(course, marks);
					student.setPerCourseMarks(courseMarks);
				}
				if(SetNotificationType.donotif = true){
					student.recieveNotif();
				}
			} else {
				System.out.println("This student is not in the course");
			}

		} else {
			System.out.println("Only an instructor may add marks for a course.");
		}
	}

	public LoggedInAuthenticatedUser performOp() {
		return null;
	}
}