package operations;

import authenticatedUsers.LoggedInAuthenticatedUser;
import customDatatypes.Marks;
import offerings.CourseOffering;
import offerings.ICourseOffering;
import registrar.ModelRegister;
import systemUsers.InstructorModel;
import systemUsers.StudentModel;

import java.util.Map;
import java.util.Scanner;

public class InstructorPrintRecord {
    public LoggedInAuthenticatedUser performOp(){
        return null;
    }
    public void performOp(LoggedInAuthenticatedUser user)
    {

        // Get the model register and then get the student from the register itself
        InstructorModel instructor = (InstructorModel) ModelRegister.getInstance().getRegisteredUser(user.getID());

        // Initialize the scanner to read user input
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter the courseID of the course you want to print a record for: ");
        String usercourseID = reader.nextLine();

        // Loop through courses enrolled in until we get to the one the user specified
        for (ICourseOffering tempCourse : instructor.getIsTutorOf())
        {
            if (tempCourse.getCourseID().equals(usercourseID))
            {

				System.out.println("\nID : " + tempCourse.getCourseID() + "\nCourse name : " + tempCourse.getCourseName() + "\nSemester : " + tempCourse.getSemester());

                
                // Get all the students in the class and then print out the record for ALL the students for THIS class only
                for (StudentModel student : tempCourse.getStudentsEnrolled()) {
                	
                	// Loop through all the courses the student is enrolled in
                	for (ICourseOffering tempCourse1 : student.getCoursesEnrolled())
                    {
                		
                		// Only continue for the course initially selected
                        if (tempCourse1.getCourseID().equals(usercourseID))
                        {
                        	
                        	// Get the marks of the student for this course
                            student.getPerCourseMarks().get(tempCourse1).initializeIterator();
                            Marks mark = student.getPerCourseMarks().get(tempCourse);
                            
                            System.out.println("\n___________________________________\n" + student.getName() + " " + student.getSurname() + "'s student record");
                            while (student.getPerCourseMarks().get(tempCourse1).hasNext())
                            {

                            	mark.next();
        						System.out.println(mark.getCurrentKey() + ": " + mark.getCurrentValue());
                            }

                            try {
                                CourseOffering course = ModelRegister.getInstance().getRegisteredCourse(usercourseID);
                                System.out.println("Final mark for this class is: ");
                                course.calculateFinalGrade(user.getID());
                            }
                            catch (Exception e){
                                System.out.println("Final mark cannot be calculated yet.");
                            }

                        }
                    }
                }
            }
        }
    }
}
