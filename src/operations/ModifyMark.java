package operations;

import authenticatedUsers.LoggedInAuthenticatedUser;
import customDatatypes.*;
import offerings.CourseOffering;
import offerings.ICourseOffering;
import registrar.ModelRegister;
import systemUsers.InstructorModel;
import systemUsers.StudentModel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class ModifyMark implements Operations {

	private String courseID, studentID, eval;
	private double mark;


	public void performOp(LoggedInAuthenticatedUser user) {

		System.out.println("You are now modifying marks\n");
		//Check to see if the user is an Instructor
		if (user.getAuthenticationToken().getUserType().equals("Instructor")) {

			// Start the scanner to get user input
			Scanner scanner = new Scanner(System.in);

			System.out.print("Enter the Course ID to modify marks to: ");
			courseID = scanner.nextLine();

			while(ModelRegister.getInstance().checkIfCourseHasAlreadyBeenCreated(courseID) == false){
				System.out.println("Please enter a valid Course ID");
				System.out.println("Enter the Course ID to add marks to: ");
				courseID = scanner.nextLine();
			}

			//Check to see if the course entered is taught by the instructor
			InstructorModel instructor = (InstructorModel)ModelRegister.getInstance().getRegisteredUser(user.getID());
			CourseOffering course = ModelRegister.getInstance().getRegisteredCourse(courseID);
			// Loop through all instructors for the course to see if the really teaches the course.
			for (InstructorModel courseInstructor : course.getInstructor()) {
				if (courseInstructor != instructor) {
					System.out.println("Only the instructor of " + courseID + " may add marks.");
					return;
				}
			}


			System.out.print("Enter the Student ID that will be having a mark modified: ");
			studentID = scanner.nextLine();

			while(ModelRegister.getInstance().checkIfUserHasAlreadyBeenCreated(studentID) == false){
				System.out.println("Please enter a valid Student ID");
				System.out.println("Enter the Student ID that will be having a mark added: ");
				studentID = scanner.nextLine();
			}

			//Find student using register and get ready to add the marks
			StudentModel student = (StudentModel)ModelRegister.getInstance().getRegisteredUser(studentID);
			System.out.println("Student selected: " + student.getName() + " " + student.getSurname());
			if (student.getCoursesEnrolled() != null && student.getCoursesEnrolled().contains(course)) {

				// If no mark has been added for this course before then we need to initialize the map for marks
				if (student.getPerCourseMarks() == null) {
					student.setPerCourseMarks(new HashMap<ICourseOffering, Marks>());
				}

				Marks marks = student.getPerCourseMarks().get(course);
				// Get user input for the evaluation strategy and the mark for it
				System.out.print("Please type the evaluation strategy (ex. Assignment x, Exam, Project, etc.: ");
				eval = scanner.nextLine();
				System.out.print("Please enter Mark: ");
				String markString = scanner.nextLine();
				mark = Double.parseDouble(markString);

				// The mark cannot be less than 0 or greater than 100
				if ((mark <= 100.0) && (mark >= 0.0)) {
					marks.addToEvalStrategy(eval, mark);
				} 
				else {
					System.out.println("Mark must be between 0 and 100.");
					return;
				}
				marks.initializeIterator();
				while(marks.hasNext()){
					if(marks.getNextEntry().getKey().equals(eval)){
						marks.getNextEntry().setValue(mark);
						System.out.println("Mark updated");
					}
					marks.next();
				}
			}
			else {
				System.out.println("This student is not in the course");
			}

		} else {
			System.out.println("Only an instructor may add marks for a course.");
		}
	}

	public LoggedInAuthenticatedUser performOp() {
		return null;
	}
}